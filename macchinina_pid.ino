#include "Ultrasonic.hpp"

const uint8_t speedRight = 3,
              speedLeft = 11,
              dirRight = 12,
              dirLeft = 13,
              brakeRight = 9,
              brakeLeft = 8,
              sensorBTrigger = 2,
              sensorBEcho = 5,
              sensorFTrigger = 6,
              sensorFEcho = 10,
              piezo = 7;


Ultrasonic frontSensor(sensorFTrigger, sensorFEcho);
Ultrasonic backSensor(sensorBTrigger, sensorBEcho);


String serialString;


int16_t speedValue = 0,
        dirValue = 0,
        speedLeftValue = 0,
        speedRightValue = 0,
        dirLeftValue = 100,
        dirRightValue = 100,
        frontDistance = 0,
        backDistance = 0,
        absSpeedValue = 0,
        setPoint = 20;


bool forwardRight =  true,
     forwardLeft = true,
     brakeLeftValue = false,
     brakeRightValue = false;

uint32_t lastTime = millis();
uint16_t dt = 100;
float P = 10, I = 0*dt, D = 2/dt;
int16_t aE = 0, lE = 0;
void setup() {
  uint8_t pins[] = {speedLeft, speedRight, dirLeft, dirRight, brakeLeft, brakeRight };
  for (uint8_t i = 0; i < sizeof(pins) / sizeof(pins[0]); i++)
    pinMode(pins[i], OUTPUT);
  Serial.begin(9600); 
  lastTime = millis();
}


void loop() {
  uint32_t timeNow = millis(); 
  if (timeNow - lastTime > dt) {
    int16_t e = setPoint - frontSensor.read();
    lastTime = timeNow;
    speedValue = P * e;
    aE += e;
    speedValue += I * aE;
    speedValue += (e - lE) * D;
    lE = e;
    Serial.print("*D" + String(speedValue) + "\n*");
    if (speedValue < 0) {
      forwardLeft = forwardRight = true;
    }
    else {
      forwardLeft = forwardRight = false;
    }
    speedValue = abs(speedValue);
    if (speedValue > 255) speedValue = 255;
    digitalWrite(dirRight, forwardRight);
    digitalWrite(dirLeft, forwardLeft);
    analogWrite(speedLeft, speedValue);
    analogWrite(speedRight, speedValue);
  }
  if (Serial.available()) {
    serialString = Serial.readStringUntil(';');
    char a = serialString[0];
    serialString.remove(0, 1);
    switch (a) {
      case 'P':
        P = serialString.toFloat();
        break;
      case 'I':
        I = serialString.toFloat() * dt;
        break;
      case 'D':
        D = serialString.toFloat() / dt;
        break;
      case 'S':
        setPoint = serialString.toInt();
        break;
      case 'T':
        I /= dt;
        D *= dt;
        dt = serialString.toInt();
        I *= dt;
        D /= dt;
        break;

    }
  }
}
