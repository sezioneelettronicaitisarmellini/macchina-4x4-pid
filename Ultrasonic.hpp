class Ultrasonic {
  public:
    Ultrasonic(uint8_t Trig, uint8_t Echo) { //The constructor take as inputs the two pins of the Ultrasonic sensor
      pinMode(Trig, OUTPUT); //set the pin I/O mode
      pinMode(Echo, INPUT);
      _Trig = Trig;
      _Echo = Echo;
      _TimeOut = 6000;    
      }
    Ultrasonic(uint8_t Trig, uint8_t Echo, uint16_t TimeOut) { //This constructor sets also the timeout for the reading
      pinMode(Trig, OUTPUT);
      pinMode(Echo, INPUT);
      _Trig = Trig;
      _Echo = Echo;
      _TimeOut = TimeOut;
    }
    uint16_t read() {
      return rawRead() / 58; //Read return the distances in centimeters 'cause rawRead returns the Microseconds and speed of sound is 0.0343 cm/us and since the wave have to go and return the result is divided by 2 so us * 0.0343 /2 is the distance but *0.0343 can be written ad /1/0.0343 which is equal to /29.15 so (us/29.15)/2 so us/58
    }
  private:
    uint32_t rawRead() { //This metod gets the the microseconds that the sound takes to go hit an object and return, if the wave takes more than the timeout the function returns 0
      digitalWrite(_Trig, LOW); //just to be sure that the trigger is in a low level 
      delayMicroseconds(2);
      digitalWrite(_Trig, HIGH); //A pulse of 10 us on the trigger pin makes the sensor emit a wave
      delayMicroseconds(10);
      digitalWrite(_Trig, LOW);
      return pulseIn(_Echo, HIGH, _TimeOut); //wait for the return of the wave
    }
    uint8_t _Trig, _Echo;
    uint16_t _TimeOut;
};
